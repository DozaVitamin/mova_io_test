//
//  MainViewController.swift
//  Mova.io.test
//
//  Created by Александр on 15.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let cellIdentifier = "cell"
    let footerIdentifier = "footer"
    
    // Initializing Realm instance and retrieving our records for each section
    let realm = try! Realm()
    lazy var firstSectionRecords: Results<Record> = { self.realm.objects(Record.self).filter("sectionId = 0") }()
    lazy var secondSectionRecords: Results<Record> = { self.realm.objects(Record.self).filter("sectionId = 1") }()

    // Dynamical URL (different on every launch), where we'll store our photos
    var photoURL: URL {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let photoURL = NSURL(fileURLWithPath: documentDirectory)
        
        return photoURL as URL
    }
    
    // Main table view, contains two sections
    let mainTableView: UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false

        return tableView
    }()
    
    let mainTableViewSections = ["User ID Photos", "Certificate Photos"]
    let photoButtonTitles = ["Add new Photo", "Add New Certificate"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        mainTableView.register(UserPhotoCell.self, forCellReuseIdentifier: self.cellIdentifier)
        mainTableView.register(Footer.self, forHeaderFooterViewReuseIdentifier: self.footerIdentifier)

        mainTableView.delegate = self
        mainTableView.dataSource = self
        view.addSubview(mainTableView)
        
        setupMainTableView()
    }

    /*
     The function is responsible for setting up anchros of main table view
     */
    func setupMainTableView() {
        mainTableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainTableView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        mainTableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        mainTableView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    // MARK: Button handlers functions

    /*
     Selector for deleteCellButton. Deletes file from local storage, realm and table
     */
    func deleteCellButtonPressed(_ cell: UITableViewCell) {
        if let deletionIndexPath = mainTableView.indexPath(for: cell) {
            
            let section = deletionIndexPath.section
            let row = deletionIndexPath.row
            let element = section == 0 ? firstSectionRecords[row] : secondSectionRecords[row]

            self.deleteImageFromLocalStorage(fileName: element.photoName)
            try! realm.write {
                realm.delete(element)
            }

            mainTableView.deleteRows(at: [deletionIndexPath], with: .automatic)
        }
    }
    
    /*
      Selector for addPhotoButton. Generates alert controller with 3 options
     */
    func addPhotoButtonPressed(sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        // First option - take new photo. It will be saved both in photo library and application
        alertController.addAction(UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default){(action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                imagePicker.allowsEditing = false
                imagePicker.view.tag = sender.tag
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        //Second option - choose photo from library. Will be saved in application
        alertController.addAction(UIAlertAction(title: "Choose from Library", style: UIAlertAction.Style.default){(action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                imagePicker.view.tag = sender.tag

                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        // Third option - cancel. Closes alert controller
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default){(action) in
            self.alertControllerBackgroundTapped()
        })
        
        // Presenting aler controller and setting up option to close on background tap
        self.present(alertController, animated: true, completion:{
            alertController.view.superview?.subviews[1].isUserInteractionEnabled = true
            alertController.view.superview?.subviews[1].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }

    /*
     Function is responsible to close alert controller view
     */
    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     Function shoots when image from camera or library picked. Saving it in realm database
     */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage

        //if the photo is just taken saving to photo library first
        if picker.sourceType == UIImagePickerController.SourceType.camera {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        
        // Saving image to local storage and preserving name in database
        let newRecord = Record()
        newRecord.photoName = saveImageToLocalStorage(image: image)!
        newRecord.sectionId = picker.view.tag

        try! realm.write {
            realm.add(newRecord)
        }
        
        self.mainTableView.reloadData()
        
        self.dismiss(animated: true, completion: nil);
    }
    
    // MARK: mainTableView delegate & datasource functions
    func numberOfSections(in tableView: UITableView) -> Int {
        var numberOfSections = 1
        
        if tableView == mainTableView {
            numberOfSections = mainTableViewSections.count // Getting quantity of our sections from property
        }
        
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var defaultNumberOfRowsInSection = 1
        
        if tableView == mainTableView {
            defaultNumberOfRowsInSection = section == 0 ? firstSectionRecords.count : secondSectionRecords.count
        }
        
        return defaultNumberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var sectionTitle = ""
        
        if tableView == mainTableView {
            sectionTitle = mainTableViewSections[section] // Getting headers names from our property
        }
        
        return sectionTitle
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: self.footerIdentifier) as! Footer
        footerView.mainViewController = self
        
        let title = photoButtonTitles[section]
        footerView.photobutton.setTitle(title, for: UIControl.State())
        footerView.photobutton.tag = section
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath as IndexPath) as! UserPhotoCell
        
        let sectionToUse = indexPath.section == 0 ? firstSectionRecords : secondSectionRecords
        let fileName = sectionToUse[indexPath.row].photoName
        cell.imageViewContainer.image = loadImageFromLocalStorage(fileName: fileName)
        cell.mainViewController = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0;
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
