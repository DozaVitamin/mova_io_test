//
//  Extensions.swift
//  Mova.io.test
//
//  Created by Александр on 16.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

// UIColor extension to init rgb without division on 255
extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
}
