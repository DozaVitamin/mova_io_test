//
//  Footer.swift
//
//  Created by Александр
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class Footer: UITableViewHeaderFooterView {
    
    var mainViewController: MainViewController?
    let photobutton = UIButton(type: .system)

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
    }
    
    func setupViews() {
        photobutton.translatesAutoresizingMaskIntoConstraints = false
        photobutton.setTitleColor(UIColor.white, for: UIControl.State())
        photobutton.backgroundColor = UIColor(r: 80, g: 101, b: 161)
        photobutton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        photobutton.titleLabel!.textAlignment = NSTextAlignment.center
        photobutton.addTarget(self, action: #selector(Footer.buttonHandler(sender:)), for: .touchUpInside)
        contentView.addSubview(photobutton)
        
        photobutton.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        photobutton.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        photobutton.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        photobutton.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true
    }

    @objc func buttonHandler(sender: UIButton) {
        mainViewController?.addPhotoButtonPressed(sender: sender)
    }
}
