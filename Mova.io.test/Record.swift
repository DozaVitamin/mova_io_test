//
//  Record.swift
//  Mova.io.test
//
//  Created by Александр on 16.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit
import RealmSwift

class Record: Object {
    @objc dynamic var photoName = ""
    @objc dynamic var sectionId = 0
}
