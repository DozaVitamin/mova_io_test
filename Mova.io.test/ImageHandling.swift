//
//  ImageHandling.swift
//  Mova.io.test
//
//  Created by Александр on 16.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

extension MainViewController {
    
    func saveImageToLocalStorage(image: UIImage) -> String? {
        let fileName = String(NSDate().timeIntervalSince1970)
        let fileURL = photoURL.appendingPathComponent(fileName)
        let imageData = image.jpegData(compressionQuality: 1.0)
        
        do {
            try imageData?.write(to: fileURL, options: Data.WritingOptions.atomic)
        } catch {
            print("Error saving image")
        }
        
        return fileName
    }
    
    func loadImageFromLocalStorage(fileName: String) -> UIImage? {
        let fileURL = photoURL.appendingPathComponent(fileName)
        
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        
        return nil
    }
    
    func deleteImageFromLocalStorage(fileName: String) {
        let fileURL = photoURL.appendingPathComponent(fileName)
        
        do {
            let fileManager = FileManager.default
            
            if fileManager.fileExists(atPath: fileURL.path) {
                try fileManager.removeItem(atPath: fileURL.path)
            } else {
                print("File does not exist")
            }
            
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
}
