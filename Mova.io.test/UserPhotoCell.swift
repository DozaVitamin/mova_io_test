//
//  UserPhotoCell.swift
//  Mova.io.test
//
//  Created by Александр on 18.09.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class UserPhotoCell: UITableViewCell {
    
    var mainViewController: MainViewController?
    
    var imageViewContainer = UIImageView()
    var deleteCellButton = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupViews()
    }
    
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
    }
    
    func setupViews() {
        imageViewContainer.contentMode = .scaleAspectFit
        imageViewContainer.translatesAutoresizingMaskIntoConstraints = false
        
        deleteCellButton.translatesAutoresizingMaskIntoConstraints = false
        deleteCellButton.setTitleColor(UIColor.red, for: UIControl.State())
        deleteCellButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        deleteCellButton.titleLabel!.textAlignment = NSTextAlignment.center
        deleteCellButton.setTitle("Delete", for: UIControl.State())
        deleteCellButton.addTarget(self, action: #selector(UserPhotoCell.deleteCellButtonPreesed), for: .touchUpInside)
        
        contentView.addSubview(imageViewContainer)
        contentView.addSubview(deleteCellButton)
        
        // setting up constraints for imageView
        imageViewContainer.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageViewContainer.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 12).isActive = true
        imageViewContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true
        imageViewContainer.widthAnchor.constraint(lessThanOrEqualToConstant: 50).isActive = true
        
        // setting up constraints for delete button
        deleteCellButton.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        deleteCellButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -12).isActive = true
        deleteCellButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    @objc func deleteCellButtonPreesed() {
        mainViewController?.deleteCellButtonPressed(self)
    }

}
